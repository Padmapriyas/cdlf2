import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductComponent }   from './product/product.component';
import { ContactComponent } from './contact/contact.component';
import { TermsComponent } from './terms/terms.component';
const routes: Routes = [
  { path: '', redirectTo: '/Product', pathMatch: 'full' },
  { path: 'Product', component: ProductComponent },
  { path: 'Contact', component: ContactComponent },
  { path: 'Terms', component: TermsComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}